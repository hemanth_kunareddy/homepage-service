/**
 * 
 */
package com.telstra.homepage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;



/**
 * @author Hemanth.Kunareddy
 *
 */
@SpringBootApplication
public class HomePageServiceMain {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SpringApplication.run(HomePageServiceMain.class, args);
	}
	
	 @Bean
	  public RestTemplate restTemplate() {
	    return new RestTemplate();
	  }

}
