/**
 * 
 */
package com.telstra.homepage.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.telstra.homepage.model.HomePage;
import com.telstra.homepage.service.HomePageService;

/**
 * @author Hemanth.Kunareddy
 *
 */
@RestController
@RequestMapping("/api/v1")
public class HomePageServiceController {
	
	@Autowired
	HomePageService homePageService;

	@GetMapping("/telstra/homepage/{zipCode}")
	public ResponseEntity<HomePage> getAggregateData(@PathVariable String zipCode){
		HomePage homePage = homePageService.getAggregateData(zipCode);
		return new ResponseEntity<>(homePage,HttpStatus.OK);
	}
}
