/**
 * 
 */
package com.telstra.homepage.model;

/**
 * @author Hemanth.Kunareddy
 *
 */
public class HomePage {

	private Weather weather;
	private Stock stock;
	
	public Weather getWeather() {
		return weather;
	}
	public void setWeather(Weather weather) {
		this.weather = weather;
	}
	public Stock getStock() {
		return stock;
	}
	public void setStock(Stock stock) {
		this.stock = stock;
	}
	
	
}
