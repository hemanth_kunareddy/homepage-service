/**
 * 
 */
package com.telstra.homepage.model;

/**
 * @author Hemanth.Kunareddy
 *
 */
public class Weather {

	private String temperature;
	private String humidiy;
	
	public String getTemperature() {
		return temperature;
	}
	public void setTemperature(String temperature) {
		this.temperature = temperature;
	}
	public String getHumidiy() {
		return humidiy;
	}
	public void setHumidiy(String humidiy) {
		this.humidiy = humidiy;
	}
	
	
}
