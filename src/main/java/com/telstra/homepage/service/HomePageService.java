/**
 * 
 */
package com.telstra.homepage.service;

import com.telstra.homepage.model.HomePage;

/**
 * @author Hemanth.Kunareddy
 *
 */

public interface HomePageService {

	
	public  HomePage getAggregateData(String zipCode);
}
