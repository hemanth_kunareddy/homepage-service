/**
 * 
 */
package com.telstra.homepage.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.telstra.homepage.model.HomePage;
import com.telstra.homepage.model.Stock;
import com.telstra.homepage.model.Weather;
import com.telstra.homepage.service.HomePageService;

/**
 * @author Hemanth.Kunareddy
 *
 */
@Service
public class HomePageServiceImpl implements HomePageService{
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Value("${weather.api.url}")
	private String weatherBaseApi;
	
	@Value("${stock.api.url}")
	private String stockBaseApi;

	@Override
	public HomePage getAggregateData(String zipCode) {
		// TODO Auto-generated method stub
		HomePage homePage = new HomePage();
		UriComponentsBuilder weatherUriBuilder = UriComponentsBuilder
		        .fromHttpUrl(weatherBaseApi).path( "/weather-updates/" + zipCode);
		System.out.println(weatherUriBuilder.toUriString());
		Weather weather= restTemplate.getForObject(weatherUriBuilder.toUriString(), Weather.class);
		UriComponentsBuilder stockUriBuilder = UriComponentsBuilder
		        .fromHttpUrl(stockBaseApi).path( "/stock-updates/" + zipCode);
		System.out.println(stockUriBuilder.toUriString());
		Stock stock = restTemplate.getForObject(stockUriBuilder.toUriString(), Stock.class);
		homePage.setWeather(weather);
		homePage.setStock(stock);
		return homePage;
	}
 
	
}
